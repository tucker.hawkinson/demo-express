const express = require("express");
const app = express();
const port = 3000;

const morgan = require("morgan");

app.use(morgan("dev"));

app.get("/", (req, res) => {
  setTimeout(() => {
    res.send("Hello Class, we are learning Openshift!");
  }, 100);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
