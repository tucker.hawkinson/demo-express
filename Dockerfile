FROM node:14.16.1-alpine3.12
RUN apk update && apk add curl

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install --only=production
# If you are building your code for production
# RUN npm ci --only=production


# Bundle app source
COPY . .

# Set Env Variables
ENV NODE_ENV production

CMD [ "node", "server.js" ]